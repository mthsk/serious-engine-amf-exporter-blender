Tomislav Kristo's old AMF exporter for Blender 2.79, updated to work with modern Blender versions.

Tested on Blender 3.1 and up to 3.4.

Please do note that only the AMF exporter is functional. Exporting AAF and ASF files or importing any of the Serious Engine ASCII formats is not supported. Instead I'd recommend using the FBX format for purposes other than exporting meshes.
