bl_info = {
    "name": "Serious Editor 3 ASCII formats",
    "author": "Tomislav Kristo & mthsk",
    "version": (0, 5),
    "blender": (2, 80, 9),
    "api": 33333,
    "location": "File > Import-Export > Serious Editor 3 ASCII",
    "description": "Exchange compatible data between Serious Editor 3 and Blender",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "https://codeberg.org/mthsk/serious-engine-amf-exporter-blender",
    "category": "Import-Export"}
    
if "bpy" in locals():
    import imp
    if "blutils" in locals():
        imp.reload(blutils)
    if "se3" in locals():
        imp.reload(se3)
    if "import_amf" in locals():
        imp.reload(import_amf)
    if "export_amf" in locals():
        imp.reload(export_amf)
    if "export_asf" in locals():
        imp.reload(export_asf)
    if "export_aaf" in locals():
        imp.reload(export_aaf)

import bpy

from bpy.props import BoolProperty

from bpy_extras.io_utils import ExportHelper
from bpy_extras.io_utils import ImportHelper

class SED_OT_ImportSEd3AMF(bpy.types.Operator, ImportHelper):
    """Import ASCII Mesh File as mesh object in current scene"""
    bl_idname = "import_mesh.sed3_amf"
    bl_label = "Import AMF"

    filename_ext = ".amf"
    filter_glob = bpy.props.StringProperty(default="*.amf", options={'HIDDEN'})
    
    @classmethod
    def poll(cls, context):
        return True
    
    def execute(self, context):
        from . import import_amf
        return import_amf.read_file(self, context)

class SED_OT_ExportSEd3AMF(bpy.types.Operator, ExportHelper):
    """Export selected meshes to ASCII Mesh File layers"""
    bl_idname = "export_mesh.sed3_amf"
    bl_label = "Export AMF"
    
    filename_ext = ".amf"
    filter_glob = bpy.props.StringProperty(default="*.amf", options={'HIDDEN'})
    
    @classmethod
    def poll(cls, context):
        active_object = context.active_object
        selected_objects = list(context.selected_objects)
        
        if not selected_objects:
            if active_object:
                selected_objects.append(active_object)
            else:
                return False
        
        for object in selected_objects:
            if object.type != 'MESH':
                return False
        
        return True
    
    def execute(self, context):
        from . import export_amf
        return export_amf.write_file(self.filepath, context)

class SED_OT_ExportSEd3ASF(bpy.types.Operator, ExportHelper):
    """Export active armature to ASCII Skeleton File"""
    bl_idname = "export_armature.sed3_asf"
    bl_label = "Export ASF"
    
    filename_ext = ".asf"
    filter_glob = bpy.props.StringProperty(default="*.asf", options={'HIDDEN'})
    
    @classmethod
    def poll(cls, context):
        return context.active_object and context.active_object.type == 'ARMATURE'
    
    def execute(self, context):
        from . import export_asf
        return export_asf.write_file(self.filepath, context)

class SED_OT_ExportSEd3AAF(bpy.types.Operator, ExportHelper):
    """Export selected object animation from playback to ASCII Animation File"""
    bl_idname = "export_animation.sed3_aaf"
    bl_label = "Export AAF"
    
    filename_ext = ".aaf"
    filter_glob = bpy.props.StringProperty(default="*.aaf", options={'HIDDEN'})
    
    animation_name = bpy.props.StringProperty(name="Animation Name", description="Animation name to export", maxlen=256)
    
    @classmethod
    def poll(self, context):
        return context.active_object != None
        
    def execute(self, context):
        from . import export_aaf
        return export_aaf.write_file(self.filepath, self.animation_name, context)

class SED_MT_file_import_sed3(bpy.types.Menu):
    bl_idname = "SED_MT_file_import_sed3"
    bl_label = "Serious Editor 3 ASCII"
    
    def draw(self, context):
        self.layout.operator(SED_OT_ImportSEd3AMF.bl_idname, text="Mesh File (.amf)")

class SED_MT_file_export_sed3(bpy.types.Menu):
    bl_idname = "SED_MT_file_export_sed3"
    bl_label = "Serious Editor 3 ASCII"
    
    def draw(self, context):
        self.layout.operator(SED_OT_ExportSEd3AMF.bl_idname, text="Mesh File (.amf)")
        #self.layout.operator(SED_OT_ExportSEd3ASF.bl_idname, text="Skeleton File (.asf)")
        #self.layout.operator(SED_OT_ExportSEd3AAF.bl_idname, text="Animation File (.aaf)")

def menu_func_import(self, context):
    self.layout.menu("SED_MT_file_import_sed3")

def menu_func_export(self, context):
    self.layout.menu("SED_MT_file_export_sed3")

classes = ( SED_OT_ExportSEd3AAF, SED_OT_ExportSEd3AMF, SED_OT_ExportSEd3ASF, SED_OT_ImportSEd3AMF, SED_MT_file_export_sed3, SED_MT_file_import_sed3 )

def register():
    from bpy.utils import register_class
    for cls in classes:
        register_class(cls)
    
    #bpy.types.TOPBAR_MT_file_import.append(menu_func_import)
    bpy.types.TOPBAR_MT_file_export.append(menu_func_export)

def unregister():
    from bpy.utils import unregister_class
    for cls in reversed(classes):
        unregister_class(cls)
    
    #bpy.types.TOPBAR_MT_file_import.remove(menu_func_import)
    bpy.types.TOPBAR_MT_file_export.remove(menu_func_export)

if __name__ == "__main__":
    register()
